package vikhaledigital.com.dialenglish;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import vikhaledigital.com.dialenglish.videolecture.VideoMainActivity;

public class CustomDictionary extends AppCompatActivity {

    private TextView mTextMessage;
    public ArrayList<String> arr;
    public ArrayAdapter adapter;
    private EditText mEditTextWord;
    String inputText;
    String searchWord;
    private FirebaseAuth mAuth;
    private Button mSearchButton;
    private ProgressBar mProgressBar2;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_lecture:
                    goToLectures();
                    finish();
                case R.id.navigation_translate:
                    Toast.makeText(CustomDictionary.this, "You are in Custom Dictionary Activity", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.navigation_logout:
                    signout();
                    finish();
            }
            return false;
        }
    };

    private void goToLectures() {
        startActivity(new Intent(getApplicationContext(), VideoMainActivity.class));
    }

    private void signout() {
        mAuth.signOut();
        startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dictionary);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            startActivity(new Intent(CustomDictionary.this, LoginActivity.class));
            finish();
        }

        mTextMessage = (TextView) findViewById(R.id.message);
        mEditTextWord = (EditText) findViewById(R.id.editTextWord);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setItemIconTintList(null);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        inputText = mEditTextWord.getText().toString();
        searchWord = inputText.toLowerCase();
        mProgressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        mProgressBar2.setVisibility(View.INVISIBLE);
        mSearchButton = findViewById(R.id.searchButton2);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchMeaning();
                dismissKeyboard(CustomDictionary.this);
            }
        });
        
    }

    private void searchMeaning() {
        mProgressBar2.setVisibility(View.VISIBLE);
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setDivider(new ColorDrawable(Color.parseColor("#FFBF00")));
        listView.setDividerHeight(2);

        final ArrayList<String> itemsInList = new ArrayList<String>();
        Query query = reference.child("lang").orderByChild("textinEnglish").equalTo(mEditTextWord.getText().toString().toLowerCase());
        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mProgressBar2.setVisibility(View.INVISIBLE);
                    // dataSnapshot is the "issue" node with all children with id 0
                    for (DataSnapshot lang : dataSnapshot.getChildren()) {
                        Log.d("DataSnapShot", lang.toString());
                        itemsInList.add("English Meaning: "+lang.child("textinEnglish").getValue().toString());
                        itemsInList.add("Marathi Meaning: "+lang.child("textinMarathi").getValue().toString());
                        itemsInList.add("Hindi Meaning: "+lang.child("textinHindi").getValue().toString());

                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(CustomDictionary.this, android.R.layout.simple_list_item_1,
                            itemsInList);
                    listView.setAdapter(arrayAdapter);
                } else {
                    Toast.makeText(CustomDictionary.this, "Not Found", Toast.LENGTH_SHORT).show();
                    mProgressBar2.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Database Error", databaseError.toString());
                Toast.makeText(CustomDictionary.this, "Network Error. Try Again Later", Toast.LENGTH_SHORT).show();
                mProgressBar2.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void dismissKeyboard(CustomDictionary activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

}
