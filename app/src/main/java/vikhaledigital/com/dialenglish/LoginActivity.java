package vikhaledigital.com.dialenglish;

import android.animation.Animator;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class LoginActivity extends AppCompatActivity {
    private EditText mEditUserName,mEditPassword;
    private LoadingButton mLoadingBtn;
    private FirebaseAuth mAuth;
    private View animateView;
    private TextView mGoToSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, SecondActivity.class));
            finish();
        }

        mLoadingBtn = (LoadingButton)findViewById(R.id.loading_btn);
        //while login failed, reset view to button with animation
        mLoadingBtn.setResetAfterFailed(true);
        mLoadingBtn.setAnimationEndListener(new LoadingButton.AnimationEndListener() {
            @Override
            public void onAnimationEnd(LoadingButton.AnimationType animationType) {
                if(animationType == LoadingButton.AnimationType.SUCCESSFUL){
                    startActivity(new Intent(LoginActivity.this, SecondActivity.class));
                }
            }
        });

        mEditUserName = (EditText) findViewById(R.id.edit_user_name);
        mEditPassword = (EditText) findViewById(R.id.edit_password);

        mGoToSignup = findViewById(R.id.goToSignUp);
        mGoToSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSignup();
            }
        });


        animateView = findViewById(R.id.animate_view);
        mLoadingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });
    }

    private void goToSignup() {
        startActivity(new Intent(LoginActivity.this, SignupActivity.class));
        finish();
    }

    private void checkLogin(){

        final String userName = mEditUserName.getText().toString();
        if(userName.length() == 0){
            mEditUserName.setError("please input user name");
            return;
        }

        final String password = mEditPassword.getText().toString();
        if(password.length() == 0){
            mEditPassword.setError("please input password");
            return;
        }

        mLoadingBtn.startLoading();
        //send login request

        //demo
        mEditUserName.setEnabled(false);
        mEditPassword.setEnabled(false);
        mLoadingBtn.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEditUserName.setEnabled(true);
                mEditPassword.setEnabled(true);

                mAuth.signInWithEmailAndPassword(userName, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                       if (task.isSuccessful()){
                           mLoadingBtn.loadingSuccessful();
                           mLoadingBtn.setAnimationEndListener(new LoadingButton.AnimationEndListener() {
                               @Override
                               public void onAnimationEnd(LoadingButton.AnimationType animationType) {
                                   toNextPage();
                               }
                           });

                       } else {
                           mLoadingBtn.loadingFailed();
                           Toast.makeText(getApplicationContext(), "Login Failed, Please Check Your Credentials", Toast.LENGTH_SHORT).show();
                       }
                    }
                });

            }
        },3000);
    }

    //add a demo activity transition animation,this is a demo implement
    private void toNextPage(){

        int cx = (mLoadingBtn.getLeft() + mLoadingBtn.getRight()) / 2;
        int cy = (mLoadingBtn.getTop() + mLoadingBtn.getBottom()) / 2;

        Animator animator = ViewAnimationUtils.createCircularReveal(animateView,cx,cy,0,getResources().getDisplayMetrics().heightPixels * 1.2f);
        animator.setDuration(500);
        animator.setInterpolator(new AccelerateInterpolator());
        animateView.setVisibility(View.VISIBLE);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startActivity(new Intent(LoginActivity.this,SecondActivity.class));
                mLoadingBtn.reset();
                animateView.setVisibility(View.INVISIBLE);
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }
}