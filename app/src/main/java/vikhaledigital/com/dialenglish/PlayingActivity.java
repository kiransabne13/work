package vikhaledigital.com.dialenglish;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;



public class PlayingActivity extends AppCompatActivity {
    private TextView mScoreView;
    private TextView mQuestionView, mOption1, mOption2, mOption3, mOption4;
    private Button mExit, mNextButton;
    private String mAnswer;
    private int mScore = 0;
    private int mQuestionNumber = 0;
    String mCorrectAnswer, mCorrectLine, checkLine;

     DatabaseReference mQRef,mC1Ref,mC2Ref,mC3Ref,mAnsRef, query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);

        mQuestionView=(TextView) findViewById(R.id.question);
        mNextButton = (Button) findViewById(R.id.nextButton);
        mOption1 = (TextView) findViewById(R.id.option1);
        mOption2 = (TextView) findViewById(R.id.option2);
        mOption3 = (TextView) findViewById(R.id.option3);
        mOption4 = (TextView) findViewById(R.id.option4);

        //Set Touch Listener
        mOption1.setOnTouchListener(new QuestionTouchListener());
        mOption2.setOnTouchListener(new QuestionTouchListener());
        mOption3.setOnTouchListener(new QuestionTouchListener());
        mOption4.setOnTouchListener(new QuestionTouchListener());

        //Set Drag Listner
//        choice1.setOnDragListener(new ChoiceDragListener());
        mQuestionView.setOnDragListener(new QuestionDragListener());



        updateQuestion();
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateQuestion();
            }
        });


    }



    private void updateQuestion() {


        String url = "https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/questionForLevel1";
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(url);

        Log.d("URL", url);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String question = dataSnapshot.getValue(String.class);

                mQuestionView.setText(question);
                mQuestionView.setTextColor(getResources().getColor(R.color.colorBlack));
                mQuestionView.setTag(null);
                mQuestionView.setTypeface(Typeface.DEFAULT);
                mQuestionView.setOnDragListener(new QuestionDragListener());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //mQuestionRef= FirebaseDatabase.getInstance("https://english-7cceb.firebaseio.com/quiz/quizLevel1s/"+ mQuestionNumber+"/questionForLevel1");
     //   mQRef= FirebaseDatabase.getInstance().getReference("https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/questionForLevel1");


    //    mQuestionNumber++;

        DatabaseReference questionCount = FirebaseDatabase.getInstance().getReference();
        questionCount.child("quizLevel1s").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getChildrenCount();
                Log.d("Children Count", String.valueOf(dataSnapshot.getChildrenCount()));
                if (mQuestionNumber <= dataSnapshot.getChildrenCount() - 1) {
                    mQuestionNumber++;
                    mNextButton.setVisibility(View.VISIBLE);
                } else {
                    mNextButton.setVisibility(View.INVISIBLE);
                    startActivity(new Intent(PlayingActivity.this, SecondActivity.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String option1url = "https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/option1";
        DatabaseReference option1Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option1url);

        option1Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option1 = dataSnapshot.getValue(String.class);
                mOption1.setText(option1);
                mOption1.setTextColor(getResources().getColor(R.color.colorBlack));
                mOption1.setVisibility(View.VISIBLE);
                mOption1.setBackground(getResources().getDrawable(R.drawable.border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String option2url = "https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/option2";
        DatabaseReference option2Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option2url);

        option2Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option2 = dataSnapshot.getValue(String.class);
                mOption2.setText(option2);
                mOption2.setTextColor(getResources().getColor(R.color.colorBlack));
                mOption2.setVisibility(View.VISIBLE);
                mOption2.setBackground(getResources().getDrawable(R.drawable.border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String option3url = "https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/option3";
        DatabaseReference option3Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option3url);

        option3Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option3 = dataSnapshot.getValue(String.class);
                mOption3.setText(option3);
                mOption3.setTextColor(getResources().getColor(R.color.colorBlack));
                mOption3.setVisibility(View.VISIBLE);
                mOption3.setBackground(getResources().getDrawable(R.drawable.border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String option4url = "https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/option4";
        DatabaseReference option4Ref = FirebaseDatabase.getInstance().getReferenceFromUrl(option4url);

        option4Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String option4 = dataSnapshot.getValue(String.class);
                mOption4.setText(option4);
                mOption4.setTextColor(getResources().getColor(R.color.colorBlack));
                mOption4.setVisibility(View.VISIBLE);
                mOption4.setBackground(getResources().getDrawable(R.drawable.border));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String correctAnswerurl = "https://english-7cceb.firebaseio.com/quizLevel1s/"+ mQuestionNumber +"/correctAnswer";
        DatabaseReference correctAnswerRef = FirebaseDatabase.getInstance().getReferenceFromUrl(correctAnswerurl);
        Log.d("Answer Url", correctAnswerurl);
        correctAnswerRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String correctAnswer = dataSnapshot.getValue(String.class);
                mCorrectAnswer = correctAnswer;

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    /**
     * QuestionTouchListener will handle touch events on draggable views
     *
     */
    private class QuestionTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                /*
                 * Drag details: we only need default behavior
                 * - clip data could be set to pass data as part of drag
                 * - shadow can be tailored
                 */
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);
                Log.d("Dedrag Start", "drag started");
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * DragListener will handle dragged views being dropped on the drop area
     * - only the drop action will have processing added to it as we are not
     * - amending the default behavior for other parts of the drag process
     *
     */
    public class QuestionDragListener implements View.OnDragListener {

        @SuppressLint("NewApi")
        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    Log.d("Drag 1", "Drag 1 Started");

                    View vi = (View) event.getLocalState();
                    TextView options = (TextView) vi;
                    //if and else statement for border color;
                    if (options.getText().toString().equals(mCorrectAnswer)) {
                        options.setTextColor(getColor(R.color.colorGreenYellow));
                        options.setBackground(getResources().getDrawable(R.drawable.correct_answer_border));
                    } else {
                        options.setTextColor(getColor(R.color.colorRed));
                        options.setBackground(getResources().getDrawable(R.drawable.wrong_answer_border));
                    }
                    vi.setVisibility(View.INVISIBLE);

                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    View item = (View) event.getLocalState();
                    TextView optionitem = (TextView) item;
                    //if and else statement for border color
                    if (optionitem.getText().toString().equals(mCorrectAnswer)) {
                        optionitem.setTextColor(getColor(R.color.colorGreenYellow));
                        optionitem.setBackground(getResources().getDrawable(R.drawable.correct_answer_border));
                    } else {
                        optionitem.setTextColor(getColor(R.color.colorRed));
                        optionitem.setBackground(getResources().getDrawable(R.drawable.wrong_answer_border));
                    }
                    //no action necessary
                    Log.d("Drag Event Entered", "Drag Event Entered");
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    Log.d("Drag Exit", "Drag Event Exited");
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    Log.d("Options Dropped", "Drag Event Action is Dropped");
                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();
                    //view dragged item is being dropped on
                    TextView dropTarget = (TextView) v;
                    //view being dragged and dropped
                    TextView dropped = (TextView) view;

                    checkLine = dropTarget.getText().toString() + dropped.getText().toString();

                    Log.d("Check Line", checkLine);

                    Log.d("Dropped Word", dropped.getText().toString());

                    //checking whether first character of dropTarget equals first character of dropped

                    if (dropped.getText().toString().equals(mCorrectAnswer)){
                        Log.d("Answer", "Congrats its Correct");
                        //  Toast.makeText(MainActivity.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                        //stop displaying the view where it was before it was dragged
                        view.setVisibility(View.INVISIBLE);
                        //update the text in the target view to reflect the data being dropped
                       dropTarget.setText("You are Correct");
                        //make it bold to highlight the fact that an item has been dropped
                        dropTarget.setTypeface(Typeface.DEFAULT_BOLD);
                        //if an item has already been dropped here, there will be a tag
                        dropTarget.setTextColor(getResources().getColor(R.color.colorGreenYellow));
                        //if and else statement for option border
                //        dropTarget.setBackground(getResources().getDrawable(R.drawable.correct_answer_border));

                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.correct_answer_custom_toast,
                                (ViewGroup) findViewById(R.id.custom_toast_container));

                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Correct Answer");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setView(layout);
                        toast.show();
//                        Log.d("SpeakOut 2", "SpeakOut 2 is executed");

                        Log.d("TTS STOp", "TTS Stopped Now");

                        //Speak correctLine

//
//                        ShapeDrawable sd = new ShapeDrawable();
//                        sd.setShape(new RectShape());
//                        sd.getPaint().setColor(getColor(R.color.colorGreenYellow));
//                        sd.getPaint().setStrokeWidth(10f);
//                        sd.getPaint().setStyle(Paint.Style.STROKE);
//
//                        dropTarget.setBackground(sd);

                        Object tag = dropTarget.getTag();
                        //if there is already an item here, set it back visible in its original place
                        if(tag!=null)
                        {
                            //the tag is the view id already dropped here
                            int existingID = (Integer)tag;
                            //set the original view visible again
                            findViewById(existingID).setVisibility(View.VISIBLE);
                        }
                        //set the tag in the target view being dropped on - to the ID of the view being dropped
                        dropTarget.setTag(dropped.getId());
                        //remove setOnDragListener by setting OnDragListener to null, so that no further drag & dropping on this TextView can be done
                        dropTarget.setOnDragListener(null);
                        updateQuestion();
                    } else {
                        dropped.setTextColor(getResources().getColor(R.color.colorRed));

                        view.setVisibility(View.VISIBLE);
                        Log.d("Answer", "It incorrect, Try again"); // wrong options are set visible again
//Custom toast for wrong answer
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.custom_toast,
                                (ViewGroup) findViewById(R.id.custom_toast_container));

                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Wrong Answer");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();

                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    Log.d("Drag Ended", "Drag Event Ended");
                    View dragendview = (View) event.getLocalState();
                    //view dragged item is being dropped on
                    TextView dragendDropTarget = (TextView) v;
                    //view being dragged and dropped
                    TextView dragendDropped = (TextView) dragendview;
                    //no action necessary
                    dragendview.setVisibility(View.VISIBLE);
                    //    dragendDropped.setVisibility(View.VISIBLE);

                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
