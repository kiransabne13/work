package vikhaledigital.com.dialenglish;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import vikhaledigital.com.dialenglish.quiz.QuizMainActivity;

public class SecondActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    TextView mTextView;
    Button mSignOutButton;
    Button mCustomDictionaryButton;
    Button mQuizActivityButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();

        mTextView = findViewById(R.id.textView);
        mSignOutButton = findViewById(R.id.buttonSignOut);
        mCustomDictionaryButton = findViewById(R.id.customDictionaryButton);
        mQuizActivityButton = findViewById(R.id.goToQuizButton);

        Log.d("User", user.getEmail());
        mTextView.setText(user.getEmail());
       // mTextView.setText(user.getEmail().toString());
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Log.d("Signed Out", "Signed Out");
                startActivity(new Intent(SecondActivity.this, LoginActivity.class));
                finish();
            }
        });

        mCustomDictionaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SecondActivity.this, CustomDictionary.class));
            }
        });

        mQuizActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SecondActivity.this, QuizMainActivity.class));
            }
        });
    }
}
