package vikhaledigital.com.dialenglish;

import android.animation.Animator;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {

    private EditText mEmailEditText, mPasswordEditText, mNameEditText;
    private FirebaseAuth mAuth;
    private LoadingButton mRegisterButton;
    private View animateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(SignupActivity.this, SecondActivity.class));
            finish();
        }

        mNameEditText = findViewById(R.id.nameEditText);
        mEmailEditText = findViewById(R.id.emailEditText);
        mPasswordEditText = findViewById(R.id.passwordEditText);
        mRegisterButton = (LoadingButton) findViewById(R.id.registerButton);
        //while Signup failed, reset view to button with animation
        mRegisterButton.setResetAfterFailed(true);
        mRegisterButton.setAnimationEndListener(new LoadingButton.AnimationEndListener() {
            @Override
            public void onAnimationEnd(LoadingButton.AnimationType animationType) {
                if(animationType == LoadingButton.AnimationType.SUCCESSFUL){
                    startActivity(new Intent(SignupActivity.this, SecondActivity.class));
                }
            }
        });
        animateView = findViewById(R.id.animate_view);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

    }

    private void registerUser() {
        final String email = mEmailEditText.getText().toString().trim();
        final String password = mPasswordEditText.getText().toString().trim();
        final String name = mNameEditText.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() < 4) {
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getApplicationContext(), "Enter Your Name", Toast.LENGTH_SHORT).show();
        }

        mRegisterButton.startLoading();
        //send login request

        //demo
        mEmailEditText.setEnabled(false);
        mPasswordEditText.setEnabled(false);
        mNameEditText.setEnabled(false);
        mRegisterButton.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEmailEditText.setEnabled(true);
                mPasswordEditText.setEnabled(true);
                mNameEditText.setEnabled(true);

                //create user
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(SignupActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (task.isSuccessful()) {
                                    mRegisterButton.loadingSuccessful();
                                    saveUserDetails(mAuth.getCurrentUser().getUid(), name, email);
                                    mRegisterButton.setAnimationEndListener(new LoadingButton.AnimationEndListener() {
                                        @Override
                                        public void onAnimationEnd(LoadingButton.AnimationType animationType) {
                                            toNextPage();
                                        }
                                    });
                                    Log.d("Task Successful", task.getResult().toString());
                                } else {
                                    mRegisterButton.loadingFailed();
                                    Toast.makeText(SignupActivity.this, "Registration failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                    Log.d("Task Failed", task.getException().toString());
                                }
                            }
                        });

            }
        },3000);

    }

    private void toNextPage() {
        int cx = (mRegisterButton.getLeft() + mRegisterButton.getRight()) / 2;
        int cy = (mRegisterButton.getTop() + mRegisterButton.getBottom()) / 2;

        Animator animator = ViewAnimationUtils.createCircularReveal(animateView,cx,cy,0,getResources().getDisplayMetrics().heightPixels * 1.2f);
        animator.setDuration(500);
        animator.setInterpolator(new AccelerateInterpolator());
        animateView.setVisibility(View.VISIBLE);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startActivity(new Intent(SignupActivity.this, SecondActivity.class));
                mRegisterButton.reset();
                animateView.setVisibility(View.INVISIBLE);
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void saveUserDetails(final String userId, final String name, final String email) {
        FirebaseUser user = mAuth.getCurrentUser();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        Map<String, String> map = new HashMap<String, String>();
        map.put("Display Name", name);
        map.put("UserID", userId);
        map.put("Email", email);
        ref.child("users").child(user.getUid()).setValue(map);

    }
}


