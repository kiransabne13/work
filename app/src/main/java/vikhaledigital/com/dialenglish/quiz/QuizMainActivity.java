package vikhaledigital.com.dialenglish.quiz;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.TestLooperManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import vikhaledigital.com.dialenglish.R;
import vikhaledigital.com.dialenglish.SecondActivity;


public class QuizMainActivity extends AppCompatActivity {
    private String correctine;
    private TextView mquestionText;
    private String Question;
    private TextView moption1, moption2, moption3, moption4;
    private String checkLine;
    private String correctAnswer;
    private Button mresetButton, mExitButton;
    FirebaseAuth mAuth;
    private TextToSpeech tts;
    private DatabaseReference questionref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_main);
        mAuth = FirebaseAuth.getInstance();
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        questionref = reference.child("quiz").child("quizLevel1s");

        questionref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               Log.d("datasnapshot", dataSnapshot.getChildren().toString());
               dataSnapshot.getChildren();
                Long count = dataSnapshot.getChildrenCount();
                Log.d("Count", Long.toString(count));
                //dataSnapshot.child("questionForLevel1");
                int i;
                for (i = 0; i < count; i++){
                    Object quextion = i;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

      //  Log.d("QuestionRef", questionref..toString());



        correctine = "Opposite of Slow is Fast";

       // Question = "Opposite of Slow is ___________";

        correctAnswer = "Fast";

        mquestionText =  findViewById(R.id.questionText);

        mquestionText.setText(Question);

        mExitButton = (Button) findViewById(R.id.exitButton);
        moption1 = findViewById(R.id.optionTextView1);
        moption2 = findViewById(R.id.optionTextView2);
        moption3 = findViewById(R.id.optionTextView3);
        moption4 = findViewById(R.id.optionTextView4);

        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });

//Set Touch Listener
        moption1.setOnTouchListener(new QuestionTouchListener());
        moption2.setOnTouchListener(new QuestionTouchListener());
        moption3.setOnTouchListener(new QuestionTouchListener());
        moption4.setOnTouchListener(new QuestionTouchListener());

        //Set Drag Listner
//        choice1.setOnDragListener(new ChoiceDragListener());
        mquestionText.setOnDragListener(new QuestionDragListener());

        // reset button


    }

    private void exit() {
        startActivity(new Intent(QuizMainActivity.this, SecondActivity.class));
        finish();
    }

//    @Override
//    public void onInit(int status) {
//        if (status == TextToSpeech.SUCCESS) {
//
//            int result = tts.setLanguage(Locale.US);
//
//            if (result == TextToSpeech.LANG_MISSING_DATA
//                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
//                Log.e("TTS", "This Language is not supported");
//            } else {
//                Log.d("SpeakOut 1", "SpeakOut 1 is executed");
//
//            }
//
//        } else {
//            Log.e("TTS", "Initilization Failed!");
//        }
//    }

//    public void speakOut() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            tts.speak(correctine, TextToSpeech.QUEUE_FLUSH, null, null);
//            Log.d("Sound", tts.toString());
//        } else {
//            tts.speak(correctine, TextToSpeech.QUEUE_FLUSH, null);
//        }
//    }

    /**
     * ChoiceTouchListener will handle touch events on draggable views
     *
     */
    private class QuestionTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                /*
                 * Drag details: we only need default behavior
                 * - clip data could be set to pass data as part of drag
                 * - shadow can be tailored
                 */
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);
                Log.d("Dedrag Start", "drag started");
                return true;
            } else {
                return false;
            }
        }
    }
    /**
     * DragListener will handle dragged views being dropped on the drop area
     * - only the drop action will have processing added to it as we are not
     * - amending the default behavior for other parts of the drag process
     *
     */

    public class QuestionDragListener implements View.OnDragListener, TextToSpeech.OnInitListener {
        private TextToSpeech tts;
        @SuppressLint("NewApi")
        @Override
        public boolean onDrag(View v, DragEvent event) {
            tts = new TextToSpeech(getApplicationContext(), this);

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    Log.d("Drag 1", "Drag 1 Started");

                    View vi = (View) event.getLocalState();
                    TextView options = (TextView) vi;
                    //if and else statement for border color;
                    if (options.getText().toString().equals(correctAnswer)) {
                        options.setTextColor(getColor(R.color.colorGreenYellow));
                        options.setBackground(getResources().getDrawable(R.drawable.correct_answer_border));
                    } else {
                        options.setTextColor(getColor(R.color.colorRed));
                        options.setBackground(getResources().getDrawable(R.drawable.wrong_answer_border));
                    }
                    vi.setVisibility(View.INVISIBLE);

                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    View item = (View) event.getLocalState();
                    TextView optionitem = (TextView) item;
                    //if and else statement for border color
                    if (optionitem.getText().toString().equals(correctAnswer)) {
                        optionitem.setTextColor(getColor(R.color.colorGreenYellow));
                        optionitem.setBackground(getResources().getDrawable(R.drawable.correct_answer_border));
                    } else {
                        optionitem.setTextColor(getColor(R.color.colorRed));
                        optionitem.setBackground(getResources().getDrawable(R.drawable.wrong_answer_border));
                    }
                    //no action necessary
                    Log.d("Drag Event Entered", "Drag Event Entered");
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    Log.d("Drag Exit", "Drag Event Exited");
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    Log.d("Options Dropped", "Drag Event Action is Dropped");
                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();
                    //view dragged item is being dropped on
                    TextView dropTarget = (TextView) v;
                    //view being dragged and dropped
                    TextView dropped = (TextView) view;

                    checkLine = dropTarget.getText().toString() + dropped.getText().toString();

                    Log.d("Check Line", checkLine);

                    Log.d("Dropped Word", dropped.getText().toString());

                    //checking whether first character of dropTarget equals first character of dropped

                    if (dropped.getText().toString().equals(correctAnswer)){
                        Log.d("Answer", "Congrats its Correct");
                        //  Toast.makeText(MainActivity.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                        //stop displaying the view where it was before it was dragged
                        view.setVisibility(View.INVISIBLE);
                        //update the text in the target view to reflect the data being dropped
                        dropTarget.setText(correctine);
                        //make it bold to highlight the fact that an item has been dropped
                        dropTarget.setTypeface(Typeface.DEFAULT_BOLD);
                        //if an item has already been dropped here, there will be a tag
                        dropTarget.setTextColor(getResources().getColor(R.color.colorGreenYellow));
                        //if and else statement for option border
                        dropTarget.setBackground(getResources().getDrawable(R.drawable.correct_answer_border));

                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.correct_answer_custom_toast,
                                (ViewGroup) findViewById(R.id.custom_toast_container));

                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Correct Answer");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setView(layout);
                        toast.show();
                        speakOut();
//                        Log.d("SpeakOut 2", "SpeakOut 2 is executed");

                        Log.d("TTS STOp", "TTS Stopped Now");

                        //Speak correctLine

//
//                        ShapeDrawable sd = new ShapeDrawable();
//                        sd.setShape(new RectShape());
//                        sd.getPaint().setColor(getColor(R.color.colorGreenYellow));
//                        sd.getPaint().setStrokeWidth(10f);
//                        sd.getPaint().setStyle(Paint.Style.STROKE);
//
//                        dropTarget.setBackground(sd);

                        Object tag = dropTarget.getTag();
                        //if there is already an item here, set it back visible in its original place
                        if(tag!=null)
                        {
                            //the tag is the view id already dropped here
                            int existingID = (Integer)tag;
                            //set the original view visible again
                            findViewById(existingID).setVisibility(View.VISIBLE);
                        }
                        //set the tag in the target view being dropped on - to the ID of the view being dropped
                        dropTarget.setTag(dropped.getId());
                        //remove setOnDragListener by setting OnDragListener to null, so that no further drag & dropping on this TextView can be done
                        dropTarget.setOnDragListener(null);
                    } else {
                        dropped.setTextColor(getResources().getColor(R.color.colorRed));

                        view.setVisibility(View.VISIBLE);
                        Log.d("Answer", "It incorrect, Try again"); // wrong options are set visible again
//Custom toast for wrong answer
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.custom_toast,
                                (ViewGroup) findViewById(R.id.custom_toast_container));

                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Wrong Answer");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();

                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    Log.d("Drag Ended", "Drag Event Ended");
                    View dragendview = (View) event.getLocalState();
                    //view dragged item is being dropped on
                    TextView dragendDropTarget = (TextView) v;
                    //view being dragged and dropped
                    TextView dragendDropped = (TextView) dragendview;
                    //no action necessary
                    dragendview.setVisibility(View.VISIBLE);
                    //    dragendDropped.setVisibility(View.VISIBLE);

                    break;
                default:
                    break;
            }
            return true;
        }

        private void speakOut() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(correctine, TextToSpeech.QUEUE_FLUSH, null, null);
                Log.d("Sound", tts.toString());
            } else {
                tts.speak(correctine, TextToSpeech.QUEUE_FLUSH, null);
            }
        }

        @Override
        public void onInit(int status) {
            if (status == TextToSpeech.SUCCESS) {

                int result = tts.setLanguage(Locale.US);

                if (result == TextToSpeech.LANG_MISSING_DATA
                        || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e("TTS", "This Language is not supported");
                } else {
                    speakOut();

                    Log.d("SpeakOut 3", "SpeakOut 1 is executed");

                }

            } else {
                Log.e("TTS", "Initilization Failed!");
            }
        }


    }
    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    public void reset(View view)
    {
        moption1.setVisibility(TextView.VISIBLE);
        moption2.setVisibility(TextView.VISIBLE);
        moption3.setVisibility(TextView.VISIBLE);
        moption4.setVisibility(TextView.VISIBLE);
        // Don't forget to shutdown tts!

        mquestionText.setText(Question);
        mquestionText.setBackground(getResources().getDrawable(R.drawable.border));

        mquestionText.setTag(null);

        mquestionText.setTypeface(Typeface.DEFAULT);
        mquestionText.setTextColor(getResources().getColor(R.color.colorBlack));
        //set options color to defualt on reset
        moption1.setTextColor(getResources().getColor(R.color.colorBlack));
        moption2.setTextColor(getResources().getColor(R.color.colorBlack));
        moption3.setTextColor(getResources().getColor(R.color.colorBlack));
        moption4.setTextColor(getResources().getColor(R.color.colorBlack));

        //set border color to normal on reset
        moption1.setBackground(getResources().getDrawable(R.drawable.border));
        moption2.setBackground(getResources().getDrawable(R.drawable.border));
        moption3.setBackground(getResources().getDrawable(R.drawable.border));
        moption4.setBackground(getResources().getDrawable(R.drawable.border));

        mquestionText.setOnDragListener(new QuestionDragListener());
    }
}
